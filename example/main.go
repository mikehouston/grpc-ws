package main

import (
	"log"
	"net/http"

	"bitbucket.org/mikehouston/grpc-ws/example/service"
	ws "bitbucket.org/mikehouston/grpc-ws/server"

	"golang.org/x/net/context"
	"golang.org/x/net/websocket"
	"google.golang.org/grpc"
)

type ServiceImpl struct {
}

func (s *ServiceImpl) Ping(ctx context.Context, msg *service.PingMessage) (*service.PingMessage, error) {
	return msg, nil
}

func main() {
	// Init websocket listener
	listener := ws.NewListener()

	// Init gRPC server
	server := grpc.NewServer()

	// Init gRPC service
	s := &ServiceImpl{}
	service.RegisterPingServer(server, s)
	go server.Serve(listener)

	// Set up HTTP handlers
	http.Handle("/grpc", websocket.Handler(listener.Handler))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "www/index.html")
	})
	http.HandleFunc("/client.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "www/client.js")
	})

	listen := "0.0.0.0:8080"
	log.Printf("Serving on http://%s\n", listen)
	if err := http.ListenAndServe(listen, nil); err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
