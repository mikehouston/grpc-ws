#gRPC over Websockets example

This is a minimal example implementing a simple ping service over websockets with GopherJS

## Generate protocol buffer client

```
go get go get github.com/golang/protobuf/protoc-gen-go
go generate ./...
```

## Compile gopherjs client

```
go get github.com/gopherjs/gopherjs
go get ./...
cd www
gopherjs build bitbucket.org/mikehouston/grpc-ws/example/client
cd -
```

## Run example

```
go run main.go
```