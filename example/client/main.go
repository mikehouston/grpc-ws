package main

import (
	"golang.org/x/net/context"

	ws "bitbucket.org/mikehouston/grpc-ws/client"
	"bitbucket.org/mikehouston/grpc-ws/example/service"

	"github.com/gopherjs/gopherjs/js"
)

func main() {
	// Get the current host
	location := js.Global.Get("window").Get("location")
	host := location.Get("host")
	protocol := location.Get("protocol")

	var path string
	if protocol.String() == "https:" {
		path = "wss://" + host.String() + "/grpc"
	} else {
		path = "ws://" + host.String() + "/grpc"
	}

	conn, err := ws.Dial(path)
	if err != nil {
		panic(err)
	}

	client := service.NewPingClient(conn)

	msg, err := client.Ping(context.Background(), &service.PingMessage{"The quick brown fox"})
	if err != nil {
		panic(err)
	}
	println(msg.Msg)
}
