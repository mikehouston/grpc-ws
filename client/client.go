package ws

import (
	"net"
	"time"

	"github.com/goxjs/websocket"
	"google.golang.org/grpc"
)

type wsConn struct {
	net.Conn
}

// LocalAddr returns the local network address.
func (c *wsConn) LocalAddr() net.Addr {
	return &net.IPAddr{IP: net.IPv4zero}
}

func WebsocketDialer(addr string, timeout time.Duration) (net.Conn, error) {
	c, err := websocket.Dial(addr, "") // Blocks until connection is established
	if err != nil {
		return nil, err
	}

	// Wrap connection to avoid LocalAddr error
	return &wsConn{c}, nil
}

func Dial(addr string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	wsopts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithDialer(WebsocketDialer)}
	allOpts := append(wsopts, opts...)
	conn, err := grpc.Dial(addr, allOpts...)
	if err != nil {
		return nil, err
	}
	return conn, err
}
